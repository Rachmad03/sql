cd C:\xampp\mysql\bin 
=> Menghubungkan ke database
    = mysql -uroot

=> MEMBUAT DATABASE DENGAN CMD <=

1. Membuat Database Dengan CMD
   Query = create database myshop;
   

2. Membuat Tabel Di Dalam Database

   A) Masuk ke database yang baru di Buat
        Query = use myshop;

   => Membuat Tabel categories
        Query = create table categories(
                    -> id int(8) auto_increment,
                    -> name varchar(255),
                    -> primary key(id)
                    -> );

    B) Membuat Tabel items
        Query = create table items (
                -> id int(8) auto_increment,
                -> name varchar(255),
                -> description varchar(255),
                -> price int(8),
                -> stock int(8),
                -> category_id int(8),
                -> primary key(id),
                -> foreign key(category_id) references categories(id)
                -> );

    C) Membuat Tabel users 
        Query = create table users (
                -> id int(8) auto_increment,
                -> name varchar(255),
                -> email varchar(255),
                -> password varchar(255),
                -> primary key(id)
                -> );


3. Memasukkan Data Pada Tabel

    A) Memasukkan Data Tabel users
        Query = insert into users(name, email, password)
                -> values("Jhon Doe", "jhon@doe.com", "jonh123"),("Jane Doe", "jane@doe.com", "jenita123");

    B) Memasukkan Data Tabel categories
        Query = insert into categories(name)
                        -> values("gadget"),("cloth"),("men"),("women"),("branded");

    C) Memasukkan Data Tabel items
        Query = insert into items(name, description, price, stock, category_id)
                -> values("Sumsang b50", "hape keren merek sumsang", 4000000, 100, 1),
                -> ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),
                -> ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);


4. Mengambil Data Dari Database

    A) Mengambil Data Dari Tabel users Kecuali passwordnya
        Query = select id, name, email from users;

    B) 
        => Mengambil Data Pada Tabel items yang memmiliki harga di atas 1 jt
            Query = select * from items where price > 1000000;

        => Mengambil Data Pada Tabel items yang memmiliki name Serupa
            Query = select * from items where name like 'uniklo%';

    C) Menampilka Data Tabel items JOIN DENGAN Tabel categories
        Query = select items.id, items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori from items inner join categories on items.category_id = categories.id;


5. Mengubah Harga Menjadi 2500000 Pada Tabel items
    Query = update items set price = "2500000" where id = 1;